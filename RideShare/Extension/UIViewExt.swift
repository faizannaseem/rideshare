//
//  UIViewExt.swift
//  RideShare
//
//  Created by Faizan on 01/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import Foundation

extension UIView
{
    func fadeTo(alphaValue: CGFloat, duration: TimeInterval)
    {
        UIView.animate(withDuration: duration) {
            self.alpha = alphaValue
        }
    }
}
