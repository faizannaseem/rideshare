
//
//  UIViewControllerExt.swift
//  RideShare
//
//  Created by Faizan on 06/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

extension UIViewController{
    
    func shouldPresentLoadingView(status: Bool)
    {
        var fadeView: UIView?
        
        if status == true
        {
            fadeView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
            fadeView?.backgroundColor =  UIColor.black
            fadeView?.alpha = 0.0
            fadeView?.tag = 99
            
            let spinner = UIActivityIndicatorView()
            spinner.color = UIColor.white
            spinner.activityIndicatorViewStyle = .whiteLarge
            spinner.center = view.center
            
            
            view.addSubview(fadeView!)
            fadeView?.addSubview(spinner)
            
            spinner.startAnimating()
            
            fadeView?.fadeTo(alphaValue: 0.7, duration: 0.2)
            
        }
        else
        {
            for subView in  view.subviews
            {
                if subView.tag == 99
                {
                    UIView.animate(withDuration: 0.2, animations: {
                        subView.alpha = 0.0
                    }, completion: { (finished) in
                        subView.removeFromSuperview()
                    })
                }
            }
        }
        
    }
    
    
}
