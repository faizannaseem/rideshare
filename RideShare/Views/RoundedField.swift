//
//  RoundedField.swift
//  RideShare
//
//  Created by Faizan on 01/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class RoundedField: UITextField {

   
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5);
    
    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = self.frame.height / 2
        
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}
