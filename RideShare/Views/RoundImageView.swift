//
//  RoundImageView.swift
//  RideShare
//
//  Created by Faizan on 01/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class RoundImageView: UIImageView {
    
    override func awakeFromNib() {
        setupView()
        
    }

    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }

}
