//
//  DriverAnnotation.swift
//  RideShare
//
//  Created by Faizan on 03/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import Foundation
import MapKit

class DriverAnnotion: NSObject, MKAnnotation
{
    dynamic var coordinate: CLLocationCoordinate2D
    var key : String
    
    init(coordinate: CLLocationCoordinate2D, key: String){
        self.coordinate = coordinate
        self.key = key
        super.init()
    }
    
    func update(annotaion: DriverAnnotion, coordinate: CLLocationCoordinate2D)
    {
        var location = self.coordinate
        location.longitude = coordinate.longitude
        location.latitude = coordinate.latitude
        UIView.animate(withDuration: 0.2) {
            self.coordinate = location
        }
    }
    
    
    
}
