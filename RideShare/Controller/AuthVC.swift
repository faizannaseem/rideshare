//
//  AuthVC.swift
//  RideShare
//
//  Created by Faizan on 01/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase

class AuthVC: UIViewController, Alertable {

    @IBOutlet weak var emailField: RoundedField!
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    @IBOutlet weak var pwdField: RoundedField!
    @IBOutlet weak var loginBtn: RoundedShadowButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        if emailField.text != nil , pwdField.text != nil
        {
            loginBtn.animateButton(shouldLoad: true, withMessage: nil)
            
            if let email = emailField.text , let pwd = pwdField.text
            {
                Auth.auth().signIn(withEmail: email, password: pwd, completion: { (user, error) in
                    
                    if error == nil
                    {
                        if self.segmentCtrl.selectedSegmentIndex == 0
                        {
                            let userData = ["provider": user!.providerID] as [String:Any]
                            DataService.instance.createFirebaseDBUser(uid: user!.uid, userData: userData, isDriver: false)
                            
                        }
                        else
                        {
                            let userData = ["provider": user!.providerID, "userIsDriver":true, "isPickUpModeEnabled": false, "driverIsOnTrip": false] as [String:Any]
                            DataService.instance.createFirebaseDBUser(uid: user!.uid, userData: userData, isDriver: true)
                        }
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                    else if let errorCode = AuthErrorCode(rawValue: error!._code) , errorCode == AuthErrorCode.userNotFound
                    {
                        Auth.auth().createUser(withEmail: email, password: pwd, completion: { (user, error) in
                            if error == nil
                            {
                                if self.segmentCtrl.selectedSegmentIndex == 0
                                {
                                    let userData = ["provider": user!.providerID] as [String:Any]
                                    DataService.instance.createFirebaseDBUser(uid: user!.uid, userData: userData, isDriver: false)
                                    
                                }
                                else
                                {
                                    let userData = ["provider": user!.providerID, "userIsDriver":true, "isPickUpModeEnabled": false, "driverIsOnTrip": false] as [String:Any]
                                    DataService.instance.createFirebaseDBUser(uid: user!.uid, userData: userData, isDriver: true)
                                }
                                self.dismiss(animated: true, completion: nil)
                            }
                            else
                            {
                                self.showAlert(_message: error?.localizedDescription ?? "")
                                print(error?.localizedDescription ?? "")
                            }
                        })
                    }
                    else
                    {
                        self.showAlert(_message: error?.localizedDescription ?? "")
                        print(error?.localizedDescription ?? "")
                    }
                })
            }
        }
        
        
        
    }
    
    
    
    
}
