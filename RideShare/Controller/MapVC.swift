//
//  MapVC.swift
//  RideShare
//
//  Created by Faizan on 01/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import MapKit
import RevealingSplashView
import Firebase


class MapVC: UIViewController, Alertable {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var rideBtn: RoundedShadowButton!
    @IBOutlet weak var centreMapBtn: UIButton!
    @IBOutlet weak var destinationField: UITextField!
    @IBOutlet weak var destinationCircleView: CircleView!
    
    let currentUser = Auth.auth().currentUser?.uid
    var tableView = UITableView()
    var matchingItems = [MKMapItem]()
    var locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    let regionRadius: Double = 1000
    var selectedPlacemark: MKPlacemark? = nil
    var route : MKRoute!
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "launchScreenIcon")!, iconInitialSize: CGSize(width: 80.0, height: 80.0), backgroundColor: UIColor.white)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        mapView.delegate = self
        configureLocationServices()
        locationManager.delegate = self
        
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
        
        self.view.addSubview(revealingSplashView)
        revealingSplashView.animationType = .heartBeat
        revealingSplashView.startAnimation()
        revealingSplashView.heartAttack = true
        
        destinationField.delegate = self
        
        
        loadDriverAnnotationFB()
        
        DataService.instance.REF_DRIVERS.observe(.value) { (snapshot) in
            self.loadDriverAnnotationFB()
        }
        
        
        UpdateService.instance.observeTrips { (tripDict) in
            if let tripDict = tripDict
            {
                let pickUpCoordinateArray = tripDict["pickUpCoordinate"] as! NSArray
                let tripKey = tripDict["passengerKey"] as! String
                let acceptanceStatus = tripDict["tripIsAccepted"] as! Bool
                
                if acceptanceStatus == false
                {
                    UpdateService.instance.driverIsAvailable(key: self.currentUser!, handler: { (available) in
                        
                        if let available = available
                        {
                            if available == true
                            {
                                let storyboard  = UIStoryboard(name: "Main", bundle: Bundle.main)
                                let pickUpVC = storyboard.instantiateViewController(withIdentifier: "PickUpVC") as? PickUpVC
                                let location = CLLocationCoordinate2D(latitude: pickUpCoordinateArray[0] as! CLLocationDegrees, longitude: pickUpCoordinateArray[1] as! CLLocationDegrees)
                                pickUpVC?.initData(coordinate: location, passengerKey: tripKey)
                                self.present(pickUpVC!, animated: true, completion: nil)
                            }
                            
                        }
                    })
                }
            }
            
            
        }
    }
    
    
    func configureLocationServices() {
        if authorizationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else {
            return
        }
    }

    @IBAction func ridePressed(_ sender: Any) {
        UpdateService.instance.updateTripCoordinate()
        rideBtn.animateButton(shouldLoad: true, withMessage: nil)
    }
    
    func centremaponUserLocation()
    {
        guard let coordinate = locationManager.location?.coordinate else { return }
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func loadDriverAnnotationFB()
    {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            if let driverSnapsot = snapshot.children.allObjects as? [DataSnapshot]{
                for driver in driverSnapsot
                {
                    if driver.hasChild("coordinate")
                    {
                        if driver.childSnapshot(forPath: "isPickUpModeEnabled").value as? Bool == true
                        {
                            if let driverDict = driver.value as? Dictionary<String, AnyObject>
                            {
                                let coordinateArray = driverDict["coordinate"] as! NSArray
                                let driverCoordinate = CLLocationCoordinate2D(latitude: coordinateArray[0] as! CLLocationDegrees, longitude: coordinateArray[1] as! CLLocationDegrees)
                                let annotation = DriverAnnotion(coordinate: driverCoordinate, key: driver.key)
                                
                                var driverIsVisible: Bool
                                {
                                    return self.mapView.annotations.contains(where: { (annotation) -> Bool in
                                        
                                        if let driverAnnotation = annotation as? DriverAnnotion
                                        {
                                            if driverAnnotation.key == driver.key{
                                                driverAnnotation.update(annotaion: driverAnnotation, coordinate: driverCoordinate)
                                                return true
                                            }
                                        }
                                        return false
                                    })
                                }
                                if !driverIsVisible
                                {
                                    self.mapView.addAnnotation(annotation)
                                }
                            }
                        }
                        else
                        {
                            for annotation in self.mapView.annotations
                            {
                                if annotation.isKind(of: DriverAnnotion.self)
                                {
                                    if let annotation = annotation as? DriverAnnotion{
                                        if annotation.key == driver.key{
                                            self.mapView.removeAnnotation(annotation)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    
    
    @IBAction func centreMapPressed(_ sender: Any) {
        if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
            centremaponUserLocation()
            centreMapBtn.fadeTo(alphaValue: 0.0, duration: 0.2)
        }
        
    }
    
    
    func OverlayPolyline(mapItem: MKMapItem)
    {
        let request = MKDirectionsRequest()
        request.source = MKMapItem.forCurrentLocation()
        request.destination = mapItem
        request.transportType = MKDirectionsTransportType.automobile
        let direction = MKDirections(request: request)
        direction.calculate { (response, error) in
            guard let response = response else {
                self.showAlert(_message: error!.localizedDescription)
                print(error.debugDescription)
                return }
            self.route = response.routes[0]
            self.mapView.add(self.route.polyline)
        }
    }
    
    
}



extension MapVC: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways
        {
            centremaponUserLocation()
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }
}


extension MapVC: MKMapViewDelegate
{
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        UpdateService.instance.updateUserLcoation(coordinate: userLocation.coordinate)
        UpdateService.instance.updateDriverLcoation(coordinate: userLocation.coordinate)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? DriverAnnotion
        {
            let identifier = "driver"
            let view: MKAnnotationView
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = UIImage(named: "driverAnnotation")
            return view
        }
        else if let annotation = annotation as? PassengerAnnotation
        {
            let identifier = "passenger"
            let view: MKAnnotationView
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = UIImage(named: "currentLocationAnnotation")
            return view
        }
        else if let annotation = annotation as? MKPointAnnotation
        {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "destination")
            if annotationView == nil
            {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "destination")
            }
            else
            {
                annotationView?.annotation = annotation
            }
            annotationView?.image = UIImage(named: "destinationAnnotation")
            return annotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        centreMapBtn.fadeTo(alphaValue: 1.0, duration: 0.2)
    }
    
    func performSearch()
    {
        matchingItems.removeAll()
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = destinationField.text
        request.region = mapView.region
        
        let search =  MKLocalSearch(request: request)
        search.start { (response, error) in
            if error != nil
            {
                self.showAlert(_message: error!.localizedDescription)
                print(error.debugDescription)
            }
            else if response!.mapItems.count == 0
            {
                self.showAlert(_message: "No results. Please search for diffrent location")
                print("No Results")
            }
            else
            {
                for mapItem in response!.mapItems
                {
                    self.matchingItems.append(mapItem)
                    
                }
                self.tableView.reloadData()
                self.shouldPresentLoadingView(status: false)
            }
        }
    }
    
    
    func dropPin(placemark: MKPlacemark)
    {
        selectedPlacemark = placemark
        
        for annotation in mapView.annotations
        {
            if annotation.isKind(of: MKPointAnnotation.self)
            {
                mapView.removeAnnotation(annotation)
            }
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate  = placemark.coordinate
        mapView.addAnnotation(annotation)
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let lineRenderer = MKPolylineRenderer(overlay: self.route.polyline)
        lineRenderer.strokeColor = #colorLiteral(red: 0.884881556, green: 0.3655636609, blue: 0.1510630548, alpha: 1)
        lineRenderer.lineWidth = 3
        shouldPresentLoadingView(status: false)
        return lineRenderer
    }
    
}

extension MapVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField ==  destinationField
        {
            tableView.frame = CGRect(x: 20, y: view.frame.height, width: view.frame.width - 40, height: view.frame.height - 170)
            tableView.layer.cornerRadius = 5.0
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
            
            tableView.delegate = self
            tableView.dataSource = self
            
            tableView.tag = 18
            tableView.rowHeight = 60
            
            view.addSubview(tableView)
            animateTableView(shouldShow: true)
            
            UIView.animate(withDuration: 0.2) {
                self.destinationCircleView.backgroundColor = UIColor.red
                self.destinationCircleView.borderColor = UIColor.init(red: 199/255, green: 0/255, blue: 0/255, alpha: 1.0)
            }
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == destinationField
        {
            performSearch()
            shouldPresentLoadingView(status: true)
            view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == destinationField
        {
            if destinationField.text == ""
            {
                UIView.animate(withDuration: 0.2) {
                    self.destinationCircleView.backgroundColor = UIColor.lightGray
                    self.destinationCircleView.borderColor = UIColor.darkGray
                }
            }
        }
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        matchingItems = []
        tableView.reloadData()
        
        DataService.instance.REF_USERS.child(currentUser!).child("tripCoordinate").removeValue()
        
        mapView.removeOverlays(mapView.overlays)
        
        for annotation in mapView.annotations
        {
            if let annotation = annotation as? MKPointAnnotation
            {
                mapView.removeAnnotation(annotation)
            }else if annotation.isKind(of: PassengerAnnotation.self)
            {
                mapView.removeAnnotation(annotation)
            }
        }
        return true
    }
    
    func animateTableView(shouldShow: Bool)
    {
        if shouldShow
        {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.frame = CGRect(x: 20, y: 170, width: self.view.frame.width - 40, height: self.view.frame.height - 170)
            })
        }
        else
        {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.frame = CGRect(x: 20, y: self.view.frame.height, width: self.view.frame.width - 40, height: self.view.frame.height - 170)
            }, completion: { (finished) in
                for subView in self.view.subviews
                {
                    if subView.tag == 18{
                        subView.removeFromSuperview()
                    }
                    
                }
            })
        }
    }
    

    
    
}


extension MapVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.textLabel?.text = matchingItems[indexPath.row].name
        cell.detailTextLabel?.text = matchingItems[indexPath.row].placemark.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        shouldPresentLoadingView(status: true)
        let passengerCoordioante = locationManager.location?.coordinate
        let passengerAnnotation = PassengerAnnotation(coordinate: passengerCoordioante!, key: currentUser!)
        mapView.addAnnotation(passengerAnnotation)
        
        destinationField.text = tableView.cellForRow(at: indexPath)?.textLabel?.text
        
        let selectedResult = matchingItems[indexPath.row]
        
        DataService.instance.REF_USERS.child(currentUser!).updateChildValues(["tripCoordinate":[selectedResult.placemark.coordinate.latitude, selectedResult.placemark.coordinate.longitude]])
        
        dropPin(placemark: selectedResult.placemark)
        OverlayPolyline(mapItem: selectedResult)
        
        animateTableView(shouldShow: false)
        view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if destinationField.text == ""
        {
            animateTableView(shouldShow: false)
        }
    }
}







