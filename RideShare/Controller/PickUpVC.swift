//
//  PickUpVC.swift
//  RideShare
//
//  Created by Faizan on 06/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class PickUpVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var regionRadius: CLLocationDistance = 1000
    var pin: MKPlacemark!
    var pickUpCoordinate: CLLocationCoordinate2D!
    var passengerKey: String!
    var locationPlacemark: MKPlacemark!
    
    let currentUser = Auth.auth().currentUser?.uid
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.layer.cornerRadius = 150
        mapView.layer.borderColor = UIColor.white.cgColor
        mapView.layer.borderWidth = 5.0
        mapView.delegate = self
        
        locationPlacemark = MKPlacemark(coordinate: pickUpCoordinate)
        dropPinFor(placemark: locationPlacemark)
        centreMapLocation(location: locationPlacemark.location!)
        
        
        DataService.instance.REF_TRIPS.child(passengerKey).observe(.value) { (snapshot) in
            if snapshot.exists()
            {
                if snapshot.childSnapshot(forPath: "tripIsAccepted").value as? Bool == true
                {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }

    func initData(coordinate: CLLocationCoordinate2D, passengerKey: String)
    {
        self.pickUpCoordinate = coordinate
        self.passengerKey = passengerKey
    }
    
    
   
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pickUpPressed(_ sender: Any) {
        UpdateService.instance.acceptTrip(passengerkey: passengerKey, driverkey: currentUser!)
        
    }
    

}



extension PickUpVC : MKMapViewDelegate
{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "PickUpPoint")
        if annotationView == nil
        {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "PickUpPoint")
        }else
        {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "destinationAnnotation")
        
        return annotationView
        
    }
    
    func centreMapLocation(location: CLLocation)
    {
        let coordinate = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinate, animated: true)
    }
    
    func dropPinFor(placemark: MKPlacemark)
    {
        pin  = placemark
        
        for annotation in mapView.annotations
        {
            mapView.removeAnnotation(annotation)
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        
        mapView.addAnnotation(annotation)
    }
    
}














