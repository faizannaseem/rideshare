//
//  LeftMenuVC.swift
//  RideShare
//
//  Created by Faizan on 01/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase

class LeftMenuVC: UIViewController {

    
    @IBOutlet var userEmaiLbl: UILabel!
    @IBOutlet var accountType: UILabel!
    @IBOutlet var pickModeLbl: UILabel!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var pickModeSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        pickModeSwitch.isOn = false
        pickModeSwitch.isHidden  = true
        pickModeLbl.isHidden = true
        
        if Auth.auth().currentUser == nil
        {
            userEmaiLbl.text = ""
            accountType.text = ""
            pickModeLbl.text = ""
            userImg.isHidden = true
            loginBtn.setTitle("Sign Up/Login", for: .normal)
        }
        else
        {
            userEmaiLbl.text = Auth.auth().currentUser?.email
            accountType.text = ""
            userImg.isHidden = false
            loginBtn.setTitle("Sign Out", for: .normal)
        }
        
        observePassengerandDriver()
    }
    
    
    func observePassengerandDriver()
    {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    if snap.key == Auth.auth().currentUser?.uid{
                        self.accountType.text = "PASSENGER"
                    }
                }
            }
            
        }
        
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshot{
                    if snap.key == Auth.auth().currentUser?.uid{
                        self.accountType.text = "DRIVER"
                        self.pickModeSwitch.isHidden = false
                        let switchStatus = snap.childSnapshot(forPath: "isPickUpModeEnabled").value as! Bool
                        self.pickModeSwitch.isOn = switchStatus
                        self.pickModeLbl.isHidden = false
                    }
                }
            }
            
        }
    }

    @IBAction func signUpPressed(_ sender: Any) {
        
        if Auth.auth().currentUser == nil
        {
            performSegue(withIdentifier: "AuthVC", sender: nil)
        }
        else
        {
            do {
                try Auth.auth().signOut()
                userEmaiLbl.text = ""
                accountType.text = ""
                userImg.isHidden = true
                pickModeSwitch.isHidden  = true
                loginBtn.setTitle("Sign Up/Login", for: .normal)
                
            }catch(let error)
            {
                print(error)
            }
        }
    }
    
    
    @IBAction func switchToggled(_ sender: Any)
    {
        if pickModeSwitch.isOn
        {
            pickModeLbl.text = "Pick Up Mode Enabled"
        DataService.instance.REF_DRIVERS.child(Auth.auth().currentUser!.uid).updateChildValues(["isPickUpModeEnabled": true])
        }
        else
        {
            pickModeLbl.text = "Pick Up Mode Disabled"
            DataService.instance.REF_DRIVERS.child(Auth.auth().currentUser!.uid).updateChildValues(["isPickUpModeEnabled": false])
        }
    }
    
    
    
    
    
    
    

}
