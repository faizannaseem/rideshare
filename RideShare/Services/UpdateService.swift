//
//  UpdateService.swift
//  RideShare
//
//  Created by Faizan on 03/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Firebase

class UpdateService
{
    static let instance = UpdateService()
    
    func updateUserLcoation(coordinate: CLLocationCoordinate2D)
    {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            
            if let userSnapshot = snapshot.children.allObjects as? [DataSnapshot]
            {
                for user in userSnapshot
                {
                    if user.key == Auth.auth().currentUser?.uid
                    {
                    DataService.instance.REF_USERS.child(user.key).updateChildValues(["coordinate":[coordinate.latitude, coordinate.longitude]])
                        
                    }
                }
            }
        }
    }
    
    func updateDriverLcoation(coordinate: CLLocationCoordinate2D)
    {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            
            if let driverSnapshot = snapshot.children.allObjects as? [DataSnapshot]
            {
                for driver in driverSnapshot
                {
                    if driver.key == Auth.auth().currentUser?.uid
                    {
                       
                       if driver.childSnapshot(forPath: "isPickUpModeEnabled").value as? Bool == true
                       {
                        DataService.instance.REF_DRIVERS.child(driver.key).updateChildValues(["coordinate":[coordinate.latitude, coordinate.longitude]])
                        }
                    }
                }
            }
        }
    }
    
    
    func updateTripCoordinate()
    {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            
            if let userSnapshot = snapshot.children.allObjects as? [DataSnapshot]
            {
                for user in userSnapshot {
                    
                    if user.key == Auth.auth().currentUser?.uid
                    {
                        if !user.hasChild("userIsDriver")
                        {
                            if let userDict = user.value as? Dictionary<String, AnyObject>
                            {
                                let pickUpArray = userDict["coordinate"] as! NSArray
                                let destinationArr = userDict["tripCoordinate"] as! NSArray
                                DataService.instance.REF_TRIPS.child(user.key).updateChildValues(["pickUpCoordinate": [pickUpArray[0],pickUpArray[1]], "destinationCoordinate": [destinationArr[0],destinationArr[1]], "tripIsAccepted": false, "passengerkey": user.key])
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    func observeTrips(handler: @escaping(_ coordinateDict : Dictionary<String,AnyObject>?) -> Void)
    {
        DataService.instance.REF_TRIPS.observe(.value) { (snapshot) in
            if let tripSnapshot = snapshot.children.allObjects as? [DataSnapshot]
            {
                for trip in tripSnapshot
                {
                    if trip.hasChild("passengerkey") && trip.hasChild("tripIsAccepted")
                    {
                        if let tripDict = trip.value as? Dictionary<String, AnyObject>{
                            handler(tripDict)
                        }
                    }
                }
            }
        }
    }
    
    
    
    func driverIsAvailable(key: String, handler: @escaping (_ status : Bool?) -> Void)
    {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            
            if let driverSnapShot = snapshot.children.allObjects as? [DataSnapshot]
            {
                for driver in driverSnapShot
                {
                    if driver.key ==  key
                    {
                        if driver.childSnapshot(forPath: "isPickUpModeEnabled").value as? Bool == true
                        {
                            if driver.childSnapshot(forPath: "driverIsOnTrip").value as? Bool == true
                            {
                                handler(false)
                            }
                            else
                            {
                                handler(true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    func acceptTrip(passengerkey: String, driverkey: String)
    {
        DataService.instance.REF_TRIPS.child(passengerkey).updateChildValues(["driverKey":driverkey, "tripIsAccepted": true])
        DataService.instance.REF_USERS.child(driverkey).updateChildValues(["driverIsOnTrip":true])
        
    }
    
    
    func cancelTrip(passengerkey: String, driverkey: String)
    {
        DataService.instance.REF_TRIPS.child(passengerkey).removeValue()
        DataService.instance.REF_USERS.child(passengerkey).child("tripCoordinate").removeValue()
        DataService.instance.REF_USERS.child(driverkey).updateChildValues(["driverIsOnTrip":false])
    }
    
    
    
    
}
